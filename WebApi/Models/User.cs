﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class User
    {
        [Key]
        public int id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public int DepartmentID { get; set; }
    }
}