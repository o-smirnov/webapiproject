﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApi.Models
{
    public class GridContext  : DbContext
    {
        public GridContext() : base("name=GridContext"){}

        public DbSet<User> Users { get; set; }

    }


}